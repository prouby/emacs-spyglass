;; Spyglass -- Major mode of spyglass server monitor.
;;
;; Copyright (C) 2018 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix gexp)
             (guix utils)
             (guix build-system emacs)
             (gnu packages)
             (gnu packages emacs)
             (gnu packages curl)
             (gnu packages tor))

(package
  (name "emacs-spyglass")
  (version "0.1.1")
  (source (local-file "." "spyglass"
                      #:recursive? #t))
  (build-system emacs-build-system)
  (propagated-inputs
   `(("curl" ,curl)
     ("tor" ,tor)))
  (native-inputs
   `(("emacs" ,emacs-minimal)))
  (inputs
   `(("emacs-bui" ,emacs-bui)))
  (home-page "https://framagit.org/prouby/emacs-fetch")
  (synopsis "Major mode of spyglass server monitor.")
  (description "This Emacs packages is major mode for spyglass server
monitor.")
  (license license:gpl3+))
