;;; spyglass-mode.el --- Emacs spyglass client.      -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Pierre-Antoine Rouby

;; Author: Pierre-Antoine Rouby <contact@parouby.fr>
;; Keywords: lisp, extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Emacs spyglass client mode.

;;; Code:

(require 'json)
(require 'bui)

;;;
;;; Variable.
;;;

(defvar spyglass-server-token "yolo"
  "Autantification token.")

(defvar spyglass-hosts-list
  '(("GNU web site (ping)"   "gnu.org"        "ping")
    ("Localhost (spyglass)"  "localhost:8080" "spyglass"))
  "List of host.")

;;;
;;; Curl
;;;

(defconst torify-path "torify"
  "torify executable name.")

(defconst curl-path "curl"
  "curl executable name.")

(defconst curl-option "--silent"
  "curl default option.")

(defun curl (protocol url &optional torify options)
  "Run curl for URL with PROTOCOL and additional OPTIONS."
  (when (eq options nil)
    (setq options ""))
  (let ((cmd (format "%s %s://%s --silent %s" curl-path protocol
                     url options)))
    (if torify
        (shell-command-to-string (concat torify-path " " cmd))
      (shell-command-to-string cmd))))

;;;
;;; Data function
;;;

(defun uptime (sec)
  "Return uptime days."
  (if (eq sec nil)
      "Unknown"
    (format-seconds "%d days"
                    (string-to-number sec))))

(defun loadavg (load)
  "Return the average load."
  (if (eq load nil)
      "0"
    (let ((data (split-string load " ")))
      (concat (nth 0 data) " "
              (nth 1 data) " "
              (nth 2 data)))))

(defun disk-str (str)
  "Return the disk free space."
  (if (equal str "")
      ""
    (let* ((disk (split-string str ","))
           (target (nth 0 disk))
           (free (nth 1 disk)))
      (format "(%s) %s    "
              target free))))

(defun disk-prettify (str)
  "Return free space for all disks."
  (let ((disks (split-string str ";")))
    (apply 'concat (mapcar 'disk-str disks))))

;;;
;;; Test function
;;;

(defun spyglass-ping (host &optional c)
  "Ping HOST, return t if HOST respond else nil.
C is count of ping, 1 by default."
  (when (eq c nil)
    (setq c 1))
  (shell-command (format "ping %s -c %d" host c)))

(defun spyglass-spyglass (host protocol &optional torify)
  "Ask HOST spyglass server with PROTOCOL. Return list of data or
nil. Set TORIFY at true if you what to use torify to contact
HOST."
  (let* ((data (curl protocol (format "%s/%s/status.json" host
                                      spyglass-server-token)
                     torify)))
    (if (equal data "")
        nil
      (json-read-from-string data))))

(defun spyglass-spyglass-https (host &optional tor)
  (spyglass-spyglass host "https" tor))

(defun spyglass-spyglass-http (host &optional tor)
  (spyglass-spyglass host "http" tor))

;;;
;;; UI
;;;

(defun spyglass-data-parser (data name)
  "Parse spyglass json data to emacs spyglass bui data."
  (if (eq data nil)
      `((name   . ,name)
        (status . "DOWN"))
    (let ((status (assoc-default 'status data))
          (uptime (uptime (number-to-string
                           (assoc-default 'uptime data))))
          (load   (loadavg (assoc-default 'load data)))
          (disk   (disk-prettify (assoc-default 'disk data))))
      `((name       . ,name)
        (status     . ,status)
        (uptime     . ,uptime)
        (load       . ,load)
        (free-space . ,disk)))))

(defun spyglass-test (host)
  "Run HOST test."
  (let* ((name (car host))
         (addr (cadr host))
         (test (caddr host)))
    (cond
     ;; Ping test
     ((equal test "ping")
      (if (eq (spyglass-ping addr) 0)   ;Default try
          (cons name
                `((name   . ,name)
                  (status . "UP")))
        (if (eq (spyglass-ping addr 3) 0) ;Else try with 3 count
            (cons name
                  `((name   . ,name)
                    (status . "UP")))
          (cons name
                `((name   . ,name)
                  (status . "DOWN"))))))
     ;; Ask spyglass server
     ((equal test "spyglass-http")
      (let* ((data (spyglass-data-parser
                    (spyglass-spyglass-http addr)
                    name)))
        (cons name data)))
     ((equal test "spyglass-https")
      (let* ((data (spyglass-data-parser
                    (spyglass-spyglass-https addr)
                    name)))
        (cons name data)))
     ;; Spyglass over tor
     ((equal test "spyglass-http-tor")
      (let* ((data (spyglass-data-parser
                    (spyglass-spyglass-http addr t)
                    name)))
        (cons name data)))
     ((equal test "spyglass-https-tor")
      (let* ((data (spyglass-data-parser
                    (spyglass-spyglass-https addr t)
                    name)))
        (cons name data)))
     ;; Unknown test
     (t (cons name
              `((name   . ,name)
                (status . "Unknown")))))))

(defun spyglass-hosts ()
  "Spyglass bui get-entries-function."
  (mapcar 'spyglass-test spyglass-hosts-list))

(bui-define-interface tests list
  :buffer-name "*Spyglass*"
  :get-entries-function 'spyglass-hosts
  :format '((name nil 30 t)
            (status nil 8 t)
            (uptime nil 12 t)
            (load nil 18 t)
            (free-space nil 30 t))
  :sort-key '(name))

;;;###autoload
(defun spyglass ()
  "Run spyglass emacs supervision."
  (interactive)
  (bui-get-display-entries 'tests 'list)
  (spyglass-mode)
  (message "spyglass: DONE."))

;;;###autoload
(defun spyglass-token-gen (&optional prefix suffix)
  "Create random token. PREFIX and SUFFIX will by use to add key
world in token."
  (interactive)
  ;; Init prefix and suffix
  (when (eq prefix nil) (setq prefix ""))
  (when (eq suffix nil) (setq suffix ""))
  ;; Format
  (let ((token (format "%s%x%s" prefix
                       (abs (random)) suffix)))
    (message token)
    token))

;;;
;;; Minor mode
;;;

(defvar spyglass-highlights
  '(("DOWN" . font-lock-warning-face)
    ("UP"   . font-lock-constant-face)
    ("\\([0-9]*\\)\\([MK]\\)"     . (1 font-lock-warning-face))
    ("\\([0-9][0-9][0-9]\\) days" . (1 font-lock-warning-face))
    ("\\([2-9][.][0-9][0-9]\\) "  . (1 font-lock-warning-face))))

;;;###autoload
(define-minor-mode spyglass-mode
  "Minor mode for displaying spyglass buffer."
  nil " spyglass" nil
  ;; Body
  (font-lock-add-keywords nil spyglass-highlights)
  (if (fboundp 'font-lock-flush)
      (font-lock-flush)
    (when font-lock-mode
      (with-no-warnings (font-lock-fontify-buffer)))))

(provide 'spyglass)
;;; spyglass-mode.el ends here
